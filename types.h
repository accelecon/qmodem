//--------------------------------------------------------------------------
// Module: Standard Data Types                                  +---------+
// Author: Michael Nagy                                         | types.h |
// Date:   03-Feb-2011                                          +---------+
//--------------------------------------------------------------------------

typedef unsigned char      byte ;
typedef unsigned short int bit16;
typedef unsigned  long int bit32;
typedef           long int int32;
typedef          char *    cptr ;
typedef const    char *    ccptr;

typedef byte * bptr;
typedef FILE * fptr;

#define FILE_GRP "/flash/opticell.grp"
#define FILE_DAT "/flash/opticell.dat"
#define FILE_MTO "/flash/opticell.mto"
#define FILE_SID "/flash/opticell.sid"
#define FILE_CMP "/flash/opticell.cmp"
#define FILE_BAT "/flash/opticell.bat"
#define FILE_QUA "/flash/opticell.qua"
#define FILE_CNT "/flash/opticell.cnt"
#define FILE_LCT "/flash/opticell.lct"
#define FILE_MAH "/flash/opticell.mah"
#define FILE_GPS "/flash/opticell.gps"

#define FILE_CFG "/flash/config.dat"

#define FILE_LOG "/flash/opticell.log"
#define FILE_TMP "/tmp/opticell.tmp"

#define FLAG_USB_INIT "/tmp/flag.usb-init"
#define FLAG_USB_KILL "/tmp/flag.usb-kill"
#define FLAG_USB_DONE "/tmp/flag.usb-done"

#define FLAG_USB_0 "/tmp/flag.usb_0"
#define FLAG_USB_1 "/tmp/flag.usb_1"
#define FLAG_USB_2 "/tmp/flag.usb_2"
#define FLAG_USB_3 "/tmp/flag.usb_3"

#define FLAG_DIAL_OK "/tmp/flag.dial_ok"
#define FLAG_PING_OK "/tmp/flag.ping_ok"
#define FLAG_THRU_OK "/tmp/flag.thru_ok"

#define FLAG_WGET_OK   "/tmp/flag.wget_ok"
#define FLAG_EMAIL_OK  "/tmp/flag.email_ok"
#define FLAG_PUTLOG_OK "/tmp/flag.putlog_ok"

#define FLAG_EMAIL_BUSY  "/tmp/flag.email_busy"

#define FLAG_THRUPUT_BUSY "/tmp/flag.thruput_busy"

#define FLAG_UPDATE_NEEDED "/tmp/flag.update_needed"
#define FLAG_UPDATE_BUSY   "/tmp/flag.update_busy"
#define FLAG_UPDATE_GETMD5 "/tmp/flag.update_getmd5"
#define FLAG_UPDATE_GETBIN "/tmp/flag.update_getbin"
#define FLAG_UPDATE_MATCH  "/tmp/flag.update_match"
#define FLAG_UPDATE_WRITE  "/tmp/flag.update_write"
#define FLAG_UPDATE_CHECK  "/tmp/flag.update_check"
#define FLAG_UPDATE_OK     "/tmp/flag.update_ok"

#define FLAG_TIMEOUT_OK "/tmp/flag.timeout_ok"

#define FLAG_GPS_EXIT "/tmp/flag.gps-exit"

#define USB_MAP "/tmp/usb.map"

#define FILE_SAV "/flash/replay.dat"

#define MODEM_MAP "/tmp/modem.map"

#define LCDPORT "/dev/ttyAT1"
#define PWRPORT "/dev/ttyAT2"

#define FILE_STDOUT "/flash/opticell.std"

#define FILE_DEBUG "/tmp/opticell.dbg"

class c_Gsm { // att/kpn/vodafone/tmobile/bellca
public:
  ccptr   Upa    ; // from config, optional
  ccptr * Upa1   ;
  ccptr * Upa2   ;
  ccptr * Upa3   ;
  ccptr * Upa4   ;
  ccptr * Iccid1 ;
  ccptr * Iccid2 ;
  ccptr * CgdCont;
  ccptr * UdInfo ;
  ccptr * SysInfo;
  ccptr * Gstatus;
  ccptr * Rscp   ;
  ccptr * Cmgf   ;
  ccptr * Ecio   ;
  ccptr * Cnti   ;
  ccptr * Cind   ;
  ccptr * Creg   ;
  ccptr * Cgreg  ;
  ccptr * Cops   ;
  ccptr * Cimi   ; // IMSI
  ccptr * I4     ;
  ccptr   ICCID  ; // from Iccid1/2
};

class c_Cdma { // sprint/verizon
public:
  ccptr * Status ;
  ccptr * Rssi   ;
  ccptr * Css    ;
  ccptr   ESN    ; // from Info
  ccptr   SID    ; // from Status
  ccptr   NID    ; // from Status
};

class c_BStat {
public:
  int   Good ; // count of good transfers
  int   Bad  ; // count of bad transfers (may be derived)
  int   Tests; // count of total transfers
  int   Total; // total transfer rate
  int   Min  ; // minimum transfer rate
  int   Avg  ; // average transfer rate
  int   Max  ; // maximum transfer rate
  ccptr Raw  ; // raw data string
};

class c_FStat {
public:
  int     Size; // file size
  c_BStat Get ; // download statistics
  c_BStat Put ; // upload statistics
};

class c_TStat {
public:
  ccptr   Vers; // thru test version
  c_BStat Recv; // thru recv statistics
  c_BStat Xmit; // thru xmit statistics
};

class c_Stats {
public:
  c_BStat Ping ; // ping stats
  c_FStat Ftp01; // file stats for 10k test file
  c_FStat Ftp02; // file stats for 100k test file
  c_FStat Ftp03; // file stats for 1000k test file
  c_TStat Thru ; // thru stats
};

enum t_State {
  e_State_Missing, e_State_Located, e_State_Active
};

enum t_Tech {
  e_Tech_Unknown, e_Tech_Cdma, e_Tech_Gsm
};

class c_Modem {
public:
  ccptr    ModemId; // modem type code
  int      Slot   ; // usb slot 0..3
  int      Handle ; // file handle for commands
  ccptr    PathCmd; // device for command port
  ccptr    PathDat; // device for dial port
  ccptr    Card   ; // modem model identifier
  ccptr    Phone  ; // modem phone number
};

class c_Carrier {
public:
  t_State  State  ;
  bool     Active ; // true if carrier in selected set
  ccptr    Code   ;
  ccptr    Name8  ; // short name (max 8 characters)
  ccptr    Name   ; // long name
  c_Modem  Modem  ;
  ccptr *  Info   ;
  ccptr *  Gmm    ;
  ccptr *  Gmr    ;
  ccptr *  Gmi    ;
  ccptr *  Gcap   ;
  ccptr *  Csq    ;
  ccptr    Vers   ; // modem firmware version
  ccptr    Timex  ; // test duration mm:ss
  ccptr    Network;
  ccptr    IMEI   ;
  ccptr    IMSI   ;
  ccptr    Mcc    ;
  ccptr    Mnc    ;
  ccptr    Cid    ; // decimal
  ccptr    Lac    ; // decimal
  ccptr    Band   ;
  bool     Fix    ; // opencellid fix available
  float    Lat    ; // opencellid latitude  (if Fix true)
  float    Lon    ; // opencellid longitude (if Fix true)
  float    Error  ; // opencellid error radius (meters)
  ccptr    ECIO   ;
  ccptr    Temp   ;
  ccptr    Channel;
  int      Ctime  ;
  t_Tech   Tech   ; // cdma or gsm
  c_Gsm    Gsm    ; // used for att/kpn/vodafone/t-mobile/bellca
  c_Cdma   Cdma   ; // used for verizon/sprint
  int      dBm    ; // signal quality -50..-112 dBm (zero for unknown)
  int      Score  ; // relative overall score 0..100
  bool     Timeout; // true if test exceeded maximum time
  bool     Cancel ; // true if user canceled test
  bool     Abort  ; // true if timeout or cancel
  c_Stats  Stats  ;
};

class c_Set {
public:
  bool  Present ; // true if one or more carriers present
  bool  Selected; // true if set of carriers is selected
  ccptr MenuText; // text to display to user on menu
  ccptr Carriers; // one or more carrier codes
};

#define GPSMODE_NONE   0 // don't try to do gps
#define GPSMODE_MODULE 1 // use internal module
#define GPSMODE_MODEM  2 // use modem if available

// Items that can be centrally-configured.

class c_Cfg {
public:
  int   Capacity     ; // mah - battery mA hour charge limit
  int   TestsExpected; // tcx - tests count expected from full charge
  int   GpsMode      ; // gps - 0=none,1=module,2=modem
  float VoltsMin     ; // vmn - battery minimum voltage (red limit)
  ccptr SiteIdPicture; // pic0 - format for siteid
  ccptr SiteIdDefault; // pic1 - default value for siteid
  ccptr SiteIdSpecial; // pic2 - siteid code to unlock unit
};

class c_Opticell {
public:
  c_Carrier Att          ;
  c_Carrier Sprint       ;
  c_Carrier Verizon      ;
  c_Carrier Kpn          ;
  c_Carrier Vodafone     ;
  c_Carrier Tmobile      ;
  c_Carrier Bellca       ;
  c_Carrier Carrier      ; // carrier currently in use
  c_Cfg     Cfg          ; // current configuration parameters
  c_Set     Set[4]       ; // optional carrier sets
  int       Battery      ; // battery percent = charge-(tests*quantum)
  int       Capacity     ; // active charge limit
  int       Quantum      ; // test quantum percent
  int       TestCount    ; // number of tests since last charge
  int       LoadCount    ; // number of loads since last save
  int       OnCharger    ; // 0/1 charger attached flag
  int       OverTemp     ; // 0/1 overtemp flag
  int       ChargeRate   ; // 0..15
  int       StartGood    ; // nonzero when StartHits is valid
  int       StartHits    ; // count of start switch presses
  int       StartAcks    ; // hitcount at last ack
  int       FormatsGood  ; // count of good status strings from uC
  int       FormatsBad   ; // count of invalid status strings from uC
  float     AmpsCharge   ; // charge current
  float     AmpsChargeMax; // charge current maximum
  float     VoltsBat     ; // battery voltage
  float     VoltsBus     ; // power bus voltage
  float     VoltsDiff0   ; // charge current sampler lo side voltage
  float     VoltsDiff1   ; // charge current sampler hi side voltage
  float     VoltsCell0   ; // battery pack cell 0 voltage
  float     VoltsCell1   ; // battery pack cell 1 voltage
  float     VoltsCell2   ; // battery pack cell 2 voltage
  float     VoltsCell3   ; // battery pack cell 3 voltage
  float     VoltsCell4   ; // battery pack cell 4 voltage
  bool      GpsCapable   ; // unit hardware (modem or module) is gps-capable
  bool      GpsMonitor   ; // gps monitor is running
  bool      GpsFixNew    ; // new gps fix available
  bool      GpsFixOld    ; // old gps fix available
  float     GpsLat       ; // gps latitude  (if GpsFix true)
  float     GpsLon       ; // gps longitude (if GpsFix true)
  float     GpsError     ; // gps error     (if GpsFix true)
  int       Fixes        ; // number of fixes available
  float     Lats         ; // sum of Fixes latitudes
  float     Lons         ; // sum of Fixes longitudes
  float     Errors       ; // sum of Fixes errors
  float     Lat          ; // latitude  (if Fixes nonzero)
  float     Lon          ; // longitude (if Fixes nonzero)
  float     Error        ; // error     (if Fixes nonzero)
  ccptr     MailTo       ; // if not defined default to 'nagy@accelecon.com'
  ccptr     Company      ; // if not defined default to 'Accelecon'
  ccptr     SiteId       ;
  bool      SiteIdNew    ; // true if SiteId changed by user
  ccptr     Menu1        ;
  ccptr     Menu2        ;
  ccptr     FirmVers     ; // opticell firmware version
  ccptr     FuelVers     ; // power controller firmware version
  ccptr     SerialNum    ; // device serial number
  int       LcdHandle    ;
  int       PwrHandle    ;
  bool      Archived     ;
  bool      FuelCom      ; // communication received from power controller
};

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
