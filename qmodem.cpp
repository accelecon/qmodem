//--------------------------------------------------------------------------
// Module: USB Modem Binary Query Utility                    +------------+
// Author: Michael Nagy                                      | qmodem.cpp |
// Date:   03-Feb-2011                                       +------------+
//--------------------------------------------------------------------------

// Serial port defaults to /dev/ttyUSB1 except for Franklin modems, where
// it defaults to /dev/ttyUSB2.  It can also be explicitly specified on
// the command line, as can vid and pid (which are otherwise taken from
// the value files in the /tmp/ directory).

#include <ctype.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "types.h"

#include "usb.h"

#ifdef USBX

struct usbx_ioctl {
	int ifno;	/* interface 0..N ; negative numbers reserved */
	int ioctl_code;	/* MUST encode size + direction of data so the
			 * macros in <asm/ioctl.h> give correct values */
	void *data;	/* param buffer (in, or out) */
};

#define USBX_MAXDRIVERNAME 255

struct usbx_getdriver {
	unsigned int interface;
	char driver[USBX_MAXDRIVERNAME + 1];
};

#define IOCTL_USBX_GETDRIVER	_IOW('U', 8, struct usbx_getdriver)
#define IOCTL_USBX_IOCTL         _IOWR('U', 18, struct usbx_ioctl)

#define IOCTL_USBX_DISCONNECT	_IO('U', 22)
#define IOCTL_USBX_CONNECT	_IO('U', 23)

#include <sys/ioctl.h>
#include <errno.h>

static int usbx_fd = 0;

static bool usbx_open( ccptr busnum, ccptr devadd) {
  static char path[32];
  sprintf( path, "/proc/bus/usb/%s/%s", busnum, devadd);
  usbx_fd = open( path, O_RDWR);
  printf( "usbx_open('%s') -> %d\n", path, usbx_fd);
  if (usbx_fd > 0) {
    printf( "usbx_open() success.\n");
    return true;
  }
  usbx_fd = 0;
  printf( "* usbx_open() failed!\n");
  return false;
}

static void usbx_close( void) {
  if (usbx_fd > 0) {
    close( usbx_fd);
    printf( "usbx_close().\n");
  }
  usbx_fd = 0;
}

static bool usbx_kernel_driver_active( int interface) {
  if (usbx_fd > 0) {
    struct usbx_getdriver getdrv;
    getdrv.interface = interface;
    int rc = ioctl( usbx_fd, IOCTL_USBX_GETDRIVER, &getdrv);
    printf( "usbx_kernel_driver_active(%d) ioctl -> %d, errno %d\n", interface, rc, errno);
    if (rc) {
      return false; // kernel driver not active
    }
    return true; // kernel driver is active
  }
  return false; // device not open
}

static bool usbx_kernel_driver_detach( int interface) {
  if (usbx_fd > 0) {
    struct usbx_ioctl command;
    command.ifno = interface;
    command.ioctl_code = IOCTL_USBX_DISCONNECT;
    command.data = NULL;
    int rc = ioctl( usbx_fd, IOCTL_USBX_IOCTL, &command);
    printf( "usbx_kernel_driver_detach(%d) ioctl -> %d, errno %d\n", interface, rc, errno);
    if (rc) {
      return false; // driver detach failed
    }
    return true; // driver detach success
  }
  return false; // device not open
}

static bool usbx_kernel_driver_attach( int interface) {
  if (usbx_fd > 0) {
    struct usbx_ioctl command;
    command.ifno = interface;
    command.ioctl_code = IOCTL_USBX_CONNECT;
    command.data = NULL;
    int rc = ioctl( usbx_fd, IOCTL_USBX_IOCTL, &command);
    printf( "usbx_kernel_driver_attach(%d) ioctl -> %d, errno %d\n", interface, rc, errno);
    if (rc < 0) {
      return false; // driver reattach failed
    } else {
      if (rc == 0) {
        return false; // driver reattach failed
    } }
    return true; // driver reattach success
  }
  return false; // device not open
}

#endif // USBX

static void DebugPrintEndpoint( const struct usb_endpoint_descriptor * endpoint) {
  printf( "          bEndpointAddress: %02xh\n", endpoint->bEndpointAddress);
  printf( "          bmAttributes: %02xh\n", endpoint->bmAttributes);
  printf( "          wMaxPacketSize: %d\n", endpoint->wMaxPacketSize);
  printf( "          bInterval: %d\n", endpoint->bInterval);
  printf( "          bRefresh: %d\n", endpoint->bRefresh);
  printf( "          bSynchAddress: %d\n", endpoint->bSynchAddress);
}

static void DebugPrintAltsetting( const struct usb_interface_descriptor * interface) {
  printf( "        bInterfaceNumber: %d\n", interface->bInterfaceNumber);
  printf( "        bAlternateSetting: %d\n", interface->bAlternateSetting);
  printf( "        bInterfaceClass: %d\n", interface->bInterfaceClass);
  printf( "        bInterfaceSubClass: %d\n", interface->bInterfaceSubClass);
  printf( "        bInterfaceProtocol: %d\n", interface->bInterfaceProtocol);
  printf( "        iInterface: %d\n", interface->iInterface);
  printf( "        bNumEndpoints: %d\n", interface->bNumEndpoints);
  for (int i = 0; i < interface->bNumEndpoints; i++) {
    printf( "        Endpoint %d\n", i);
    DebugPrintEndpoint( &interface->endpoint[i]);
} }

static void DebugPrintInterface( const struct usb_interface * interface) {
  printf( "      num_altsetting: %d\n", interface->num_altsetting);
  for (int i = 0; i < interface->num_altsetting; i++) {
    printf( "      altsetting %d\n", i);
    DebugPrintAltsetting( &interface->altsetting[i]);
} }

static void DebugPrintConfiguration( const struct usb_config_descriptor * config) {
  printf( "    wTotalLength: %d\n", config->wTotalLength);
  printf( "    bConfigurationValue: %d\n", config->bConfigurationValue);
  printf( "    iConfiguration: %d\n", config->iConfiguration);
  printf( "    bmAttributes: %02xh\n", config->bmAttributes);
  printf( "    MaxPower: %d\n", config->MaxPower);
  printf( "    bNumInterfaces: %d\n", config->bNumInterfaces);
  for (int i = 0; i < config->bNumInterfaces; i++) {
    printf( "    Interface %d\n", i);
    DebugPrintInterface( &config->interface[i]);
} }

static int DebugPrint( void) {
  struct usb_bus * bus;
  struct usb_device * dev;
  usb_init();
  usb_find_busses();
  usb_find_devices();
  printf( "bus/device idVendor/idProduct\n");
  for (bus = usb_busses; bus; bus = bus->next) {
    for (dev = bus->devices; dev; dev = dev->next) {
      int ret, i;
      char string[256];
      usb_dev_handle *udev;
      printf( "%s/%s %04X/%04X\n", bus->dirname, dev->filename, dev->descriptor.idVendor, dev->descriptor.idProduct);
      if (udev = usb_open(dev)) {
        if (dev->descriptor.iManufacturer) {
          ret = usb_get_string_simple(udev, dev->descriptor.iManufacturer, string, sizeof(string));
          if (ret > 0) {
            printf( "  Manufacturer : %s\n", string);
          } else {
            printf( "* Unable to fetch manufacturer string!\n");
        } }
        if (dev->descriptor.iProduct) {
          ret = usb_get_string_simple(udev, dev->descriptor.iProduct, string, sizeof(string));
          if (ret > 0) {
            printf( "  Product : %s\n", string);
          } else {
            printf( "* Unable to fetch product string!\n");
        } }
        if (dev->descriptor.iSerialNumber) {
          ret = usb_get_string_simple(udev, dev->descriptor.iSerialNumber, string, sizeof(string));
          if (ret > 0) {
            printf( "  Serial Number: %s\n", string);
          } else {
            printf( "* Unable to fetch serial number string!\n");
        } }
        usb_close( udev);
      }
      if (!dev->config) {
        printf( "* Couldn't retrieve descriptors!\n");
        continue;
      }
      printf( "  bNumConfigurations: %d\n", dev->descriptor.bNumConfigurations);
      for (i = 0; i < dev->descriptor.bNumConfigurations; i++) {
        printf( "  Configuration %d\n", i);
        DebugPrintConfiguration( &dev->config[i]);
  } } }
  return 0;
}

struct usb_device     * UsbDevice    = NULL;
struct usb_dev_handle * UsbDevHandle = NULL;

// If we get a valid csq value, set SigRc to something like 'sig=3' and
// update the various /tmp/value.rssi_* files in the same way that the
// /nb/rssi.sh script would have done.

static ccptr SigRc = "err";

static bit16 ValueExportCsq = 0;
static bit16 ValueExportDbm = 0;
static bit16 ValueExportPct = 0;

// Do the actual export.

#define TMP_FLAG_CONNECTED         "/tmp/flag.connected"

static bool UtilFileExists(ccptr Filename) {
  static struct stat sb;
  return (stat(Filename, &sb) == 0) ? true : false;
}

static void ExportCsqDbmSignal( void) {
  ccptr Quality;

  // Changes here need to be reflected in main.cpp, rssi.sh and nbfx_notifier.sh
  // note the logic here is a little different as we are using implied
  // negative numbers :-(

  if (ValueExportDbm > 109) { Quality = "Bad";      } else
  if (ValueExportDbm > 93)  { Quality = "Marginal"; } else 
  if (ValueExportDbm > 83)  { Quality = "OK";       } else
  if (ValueExportDbm > 73)  { Quality = "Good";     } else 
  {                           Quality = "Excellent";}

  
  if (ValueExportDbm <= 75)  { SigRc = "sig=5"; } else 
  if (ValueExportDbm <= 83)  { SigRc = "sig=4"; } else 
  if (ValueExportDbm <= 95)  { SigRc = "sig=3"; } else 
  if (ValueExportDbm <= 105) { SigRc = "sig=2"; } else 
  if (ValueExportDbm <= 110) { SigRc = "sig=1"; } else 
  {                            SigRc = UtilFileExists( TMP_FLAG_CONNECTED) ?
                                       "sig=1" : "sig=0"; }
  
  if (ValueExportDbm >= 113)
    ValueExportPct = 0;
  else if (ValueExportDbm <= 51)
    ValueExportPct = 100;
  else
    ValueExportPct = ((113-ValueExportDbm)*100)/62;

  static char Signal[80];
  sprintf( Signal, "%s (-%d dBm) (%d%%)", Quality, ValueExportDbm, ValueExportPct);
  if (fptr f = fopen( "/tmp/value.rssi_csq", "w")) { fprintf( f,  "%d\n", ValueExportCsq); fclose( f); }
  if (fptr f = fopen( "/tmp/value.rssi_dbm", "w")) { fprintf( f, "-%d\n", ValueExportDbm); fclose( f); }
  if (fptr f = fopen( "/tmp/value.rssi_pct", "w")) { fprintf( f, "-%d\n", ValueExportPct); fclose( f); }
  if (fptr f = fopen( "/tmp/value.signal"  , "w")) { fprintf( f,  "%s\n", Signal        ); fclose( f); }
  printf( "Export Signal '%s'\n", Signal);
}

// Keep this logic in sync with /nb/rssi.sh!

static bool CommitCsqDbm( bit16 Csq, bit16 Dbm) {
  ValueExportCsq = Csq;
  ValueExportDbm = Dbm;
  return true;
}

// Valid dbm values are 51..113, with an implied negative sign.

static bool ExportDbm( bit16 Dbm) {
  if ((Dbm >= 51) && (Dbm <= 113)) {
    bit16 Csq = (113 - Dbm) / 2;
    return CommitCsqDbm( Csq, Dbm);
  }
  return false;
}

// Valid csq values are 0..31.

static bool ExportCsq( bit16 Csq) {
  if (Csq <= 31) {
    bit16 Dbm = 113 - (2 * Csq);
    return CommitCsqDbm( Csq, Dbm);
  }
  return false;
}

// Sierra cns reports rssi in -1.0 dbm units.

static bool SwExportRssi( bit16 Rssi) {
  return ExportDbm( Rssi);
}

// Sierra cns reports active as 1 for an activated modem.

static bool SwExportActive( bit16 Active) {
  printf( "SwExportActive(%d)\n", Active);
  return false;
}

// Sierra cns reports rssi in -1.0 dbm units, ecio in -0.5 dbm units

static bool SwExportEcio( bit16 RssiCdma, bit16 EcioCdma, bit16 RssiEvdo, bit16 EcioEvdo) {
  printf( "SwExportEcio(%d,%d,%d,%d)\n", RssiCdma, EcioCdma, RssiEvdo, EcioEvdo);
  return false;
}

// Sleep from 0 to 999 milliseconds.

static void Sleep_mS( int ms) {
  const timespec ts = { 0, ms * 1000000 };
  nanosleep( &ts, NULL);
}

static bool SerialClose( int & PortHandle) {
  if (PortHandle > 0) {
    Sleep_mS( 20);
    close( PortHandle);
    PortHandle = 0;
    return true;
  }
  PortHandle = 0;
  return false;
}

static bool SerialOpen( int & PortHandle, ccptr SerialDevice) {
  if (PortHandle) {
    printf( "### SerialOpenErr 1 [%s]\n", SerialDevice);
    return false;
  }
  PortHandle = open( SerialDevice, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (PortHandle <= 0) {
    PortHandle = 0;
    printf( "### SerialOpenErr 2 [%s]\n", SerialDevice);
    return false;
  }
  int BaudRateCode = B115200;
  struct termios DeviceAttributes;
  if (tcgetattr( PortHandle, &DeviceAttributes)) {
    SerialClose( PortHandle);
    printf( "### SerialOpenErr 3 [%s]\n", SerialDevice);
    return false;
  }
  DeviceAttributes.c_iflag = 0;
  DeviceAttributes.c_oflag = 0;
  DeviceAttributes.c_cflag = CREAD | CS8 | CLOCAL; // enable receiver, 8n1, no hardware handshaking
  DeviceAttributes.c_lflag = 0;
  cfsetospeed( &DeviceAttributes, BaudRateCode); // output baud rate
  cfsetispeed( &DeviceAttributes, BaudRateCode); //  input baud rate
  if (tcsetattr( PortHandle, TCSANOW, &DeviceAttributes)) {
    SerialClose( PortHandle);
    printf( "### SerialOpenErr 4 [%s]\n", SerialDevice);
    return false;
  }
  return true;
}

static bool SerialGet( int PortHandle, byte & RecvByte, int TimeoutMs) {
  if (read( PortHandle, &RecvByte, 1) == 1) {
    return true;
  }
  if (TimeoutMs) {
    Sleep_mS( TimeoutMs);
    return SerialGet( PortHandle, RecvByte, 0);
  }
  RecvByte = 0;
  return false;
}

//----------------------------------------------------------------------------
// Hex dump a binary buffer.
//----------------------------------------------------------------------------

static void HexDump( ccptr Tag, bptr Data, bit16 Length) {
  if (Data && Length) {
    printf( "%s [%d]: ", Tag, Length);
    for (bit16 i = 0; i < Length; i++) {
      if (i % 32 == 0) printf( "\n");
      printf( " %2.2x", Data[i]);
    }
    printf( "\n");
} }

//----------------------------------------------------------------------------
// Calculate the 16-bit crc of a message.  Given a message m of length n in a
// buffer of length n+3, the crc is calculated over m[0]..m[n-1], then m[n] is
// filled in with the lsb of the crc, m[n+1] with the msb, and m[n+2] with
// 0x7e, the message terminator.
//----------------------------------------------------------------------------

static bit16 Crc16( bptr Data, bit16 Length) {
  bit32 NewCrc;
  byte Byte;
  int32 BitLength;
  NewCrc = 0x00F32100;
  while (Length--) {
    Byte = *Data++;
    for (BitLength = 0; BitLength <= 7; BitLength++) {
      NewCrc >>= 1;
      if (Byte & 0x01) {
        NewCrc |= 0x00800000;
      }
      if (NewCrc & 0x00000080) {
        NewCrc ^= 0x00840800;
      }
      Byte >>= 1;
  } }
  for (BitLength = 0; BitLength <= 15; BitLength++) {
    NewCrc >>= 1;
    if (NewCrc & 0x00000080) {
      NewCrc ^= 0x00840800;
  } }
  return (~NewCrc) >> 8;
}

static byte  RecvData[1024];
static bit16 RecvLength = 0;
static bool  RecvEscape = false;

static void RecvFlush( void) {
  RecvLength = 0;
  RecvEscape = false;
}

//-----------------------------------------------------------------------------
// Franklin/Icera Serial Device Support
//-----------------------------------------------------------------------------

// There seems to be an artificial 64-byte limit to an individual block write,
// so we will transparently implement that here.  We will also escape special
// characters as needed and add the frame end mark.

static bool FwPutBuffer( int PortHandle, bptr RawData, int RawLength) {
  if (PortHandle > 0) {
    static char EscData[256];
    int EscLength = 0;
    for (int i = 0; i < RawLength; i++) {
      switch (RawData[i]) {
        case 0x7d: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5d; break;
        case 0x7e: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5e; break;
        default  : EscData[EscLength++] = RawData[i];
    } }
    EscData[EscLength++] = 0x7e;
    int Block = (EscLength > 64) ? 64 : EscLength;
    int EscOffset = 0;
    while (EscLength && (write( PortHandle, EscData+EscOffset, Block) == Block)) {
      EscOffset += Block;
      EscLength -= Block;
      Sleep_mS( 1);
      if (Block > EscLength) {
        Block = EscLength;
    } }
    if (EscLength == 0) {
      return true;
  } }
  return false;
}

// Return true if Data is a valid message with correct crc and a
// frame end mark.

static bool FwMsgCheck( bptr RecvData, bit16 RecvLength) {
  bit16 crc= Crc16( RecvData, RecvLength-3);
  byte CrcLo = crc & 0xff;
  byte CrcHi = crc >> 8;
  byte Frame = 0x7e;
  if (RecvData[RecvLength-3] != CrcLo) return false;
  if (RecvData[RecvLength-2] != CrcHi) return false;
  if (RecvData[RecvLength-1] != Frame) return false;
  return true;
}

// Put a byte into the receive buffer, un-escaping it as necessary.
// The escape transformations are:
//
//    7d 5d -> 7d
//    7d 5e -> 7e

#define FwRspLen_Dbm 31 // includes crc/eof
#define FwRspOff_Dbm 18

static bool FwGetByte( byte RecvByte, bptr XmitData, bit16 XmitLength) {
  bool rc = false;
  if (RecvEscape) {
    RecvEscape = false;
    if (RecvByte == 0x5d) RecvByte = 0x7d;
    if (RecvByte == 0x5e) RecvByte = 0x7e;
  } else {
    if (RecvByte == 0x7d) {
      RecvEscape = true;
      return rc;
  } }
  if (RecvLength < sizeof( RecvData)) {
    RecvData[RecvLength++] = RecvByte;
  }
  if (RecvByte == 0x7e) {
    HexDump( "Xmit", XmitData, XmitLength);
    HexDump( "Recv", RecvData, RecvLength);
    if (FwMsgCheck( RecvData, RecvLength)) {
      switch (RecvLength) {
        case FwRspLen_Dbm:
          if (memcmp( RecvData, XmitData, 4) == 0) {
            bit16 Dbm = RecvData[FwRspOff_Dbm];
            rc = ExportDbm( Dbm);
          }
          break;
    } }
    RecvFlush();
  }
  return rc;
}

// Xmit a message after calculating and appending the crc.  The
// FwPutBuffer routine will escape and frame it.

static bool FwMsgXmit( int Modem, bptr XmitData, bit16 XmitLength) {
  static byte CrcData[256];
  memcpy( CrcData, XmitData, XmitLength);
  bit16 crc= Crc16( XmitData, XmitLength);
  CrcData[XmitLength++] = crc & 0xff;
  CrcData[XmitLength++] = crc >> 8;
  return FwPutBuffer( Modem, CrcData, XmitLength);
}

// Receive a message.  Processing will occur at a low level when the frame
// end character is received.

static bool FwMsgRecv( int Modem, bptr XmitData, bit16 XmitLength) {
  byte RecvByte;
  RecvFlush();
  while (SerialGet( Modem, RecvByte, 10)) {
    if (FwGetByte( RecvByte, XmitData, XmitLength)) {
      return true;
  } }
  return false;
}

// Discard all receive Data, then send a snapshot and interpret any response.

static byte FwGetDbm[] = { // 18 bytes
  0x00, 0x0b, 0x00, 0x0d, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x05, 0x00, 0x01, 0x7c, 0xe4, 0x07
};

static bool FwQuery( int Modem, bptr XmitData, int XmitLength) {
  byte RecvByte;
  while (SerialGet( Modem, RecvByte, 10)) ;
  if (FwMsgXmit( Modem, XmitData, XmitLength)) {
    return FwMsgRecv( Modem, XmitData, XmitLength);
  }
  return false;
}

//-----------------------------------------------------------------------------
// Franklin/Icera Interface/Endpoint Support
//-----------------------------------------------------------------------------

// Skip any initial frame delimiters and verify the presence of a terminal
// frame delimiter.  Return true with successfully unframed message at start
// of buffer (in-place) and unframed message length.

static bool FwEpMsgFrameCheck( bptr RecvData, bit16 & RecvLength) {
  if (RecvData) {
    bptr RawData = RecvData;
    bit16 RawLength = RecvLength;
    RecvLength = 0;
    while (RawLength--) {
      byte RawByte = *RawData++;
      if (RawByte == 0x7e) {
        if (RecvLength) return true;
      } else {
        RecvData[RecvLength++] = RawByte;
  } } }
  return false;
}

// Unescape a buffer in-place and adjust the message length.  The unescape
// transformations are:
//
//    7d 5d -> 7d
//    7d 5e -> 7e

static void FwEpMsgDecode( bptr RecvData, bit16 & RecvLength) {
  if (RecvData) {
    bit16 EncodeLength = RecvLength;
    bptr EncodeData = RecvData;
    RecvLength = 0;
    while (EncodeLength--) {
      byte EncodeByte = *EncodeData++;
      if (EncodeByte == 0x7d) {
        if (EncodeLength) {
          EncodeLength--;
          EncodeByte = *EncodeData++;
          if (EncodeByte == 0x5d) RecvData[RecvLength++] = 0x7d;
          if (EncodeByte == 0x5e) RecvData[RecvLength++] = 0x7e;
        }
      } else {
        RecvData[RecvLength++] = EncodeByte;
} } } }

// Return true and adjust the message length if the crc is correct.

static bool FwEpMsgCrcCheck( bptr RecvData, bit16 & RecvLength) {
  if (RecvData && (RecvLength > 2)) {
    bit16 Crc = Crc16( RecvData, RecvLength-2);
    byte CrcLo = Crc & 0xff;
    byte CrcHi = Crc >> 8;
    if ((RecvData[RecvLength-2] == CrcLo) &&
        (RecvData[RecvLength-1] == CrcHi)) {
      RecvLength -= 2; // discard crc
      return true;
  } }
  return false;
}

// Expected messages for various modem types.

#define FwEpRspLen301_Dbm 15 // 15 bytes, unescaped, without crc or framing
#define FwEpRspOff301_Dbm  5

#define FwEpRspLen600_Dbm 28 // 28 bytes, unescaped, without crc or framing
#define FwEpRspOff600_Dbm 18

// Skipping any initial frame delimiter, then unescape and check the crc.

static bool FwEpMsgRecv( int EndpointRecv, bptr XmitData) {
  bool rc = false;
  static byte RecvData[128];
  int RecvMax = sizeof( RecvData);
  bit16 RecvLength = usb_bulk_read( UsbDevHandle, EndpointRecv, (cptr) RecvData, RecvMax, 100);
  if (RecvLength > 0) {
    if (FwEpMsgFrameCheck( RecvData, RecvLength)) {
      FwEpMsgDecode( RecvData, RecvLength);
      if (FwEpMsgCrcCheck( RecvData, RecvLength)) {
        HexDump( "Recv", RecvData, RecvLength);
        switch (RecvLength) {
          case FwEpRspLen301_Dbm:
            if (memcmp( RecvData, XmitData, 1) == 0) {
              bit16 Dbm = RecvData[FwEpRspOff301_Dbm];
              rc = ExportDbm( Dbm);
            }
            break;
          case FwEpRspLen600_Dbm:
            if (memcmp( RecvData, XmitData, 4) == 0) {
              bit16 Dbm = RecvData[FwEpRspOff600_Dbm];
              rc = ExportDbm( Dbm);
            }
            break;
  } } } }
  return rc;
}

// Escape a buffer, bracket it with frame delimiters, and send it.

static bool FwEpPutBuffer( int EndpointXmit, bptr XmitData, bit16 XmitLength) {
  static byte EncodeData[256];
  bit16 EncodeLength = 0;
  EncodeData[EncodeLength++] = 0x7e;
  for (int i = 0; i < XmitLength; i++) {
    switch (XmitData[i]) {
      case 0x7d: EncodeData[EncodeLength++] = 0x7d; EncodeData[EncodeLength++] = 0x5d; break;
      case 0x7e: EncodeData[EncodeLength++] = 0x7d; EncodeData[EncodeLength++] = 0x5e; break;
      default  : EncodeData[EncodeLength++] = XmitData[i];
  } }
  EncodeData[EncodeLength++] = 0x7e;
  int XmitRc = usb_bulk_write( UsbDevHandle, EndpointXmit, (cptr) EncodeData, EncodeLength, 0);
  if (XmitRc == EncodeLength) {
    return true;
  } else {
    printf( "* Bulk write returned %d (expected %d)!\n", XmitRc, EncodeLength);
  }
  return false;
}

// Xmit a message after calculating and appending the crc.  The
// FwEpPutBuffer routine will escape and frame it.

static bool FwEpMsgXmit( int EndpointXmit, bptr XmitData, bit16 XmitLength) {
  HexDump( "Xmit", XmitData, XmitLength);
  static byte CrcData[256];
  memcpy( CrcData, XmitData, XmitLength);
  bit16 Crc= Crc16( XmitData, XmitLength);
  CrcData[XmitLength++] = Crc & 0xff;
  CrcData[XmitLength++] = Crc >> 8;
  return FwEpPutBuffer( EndpointXmit, CrcData, XmitLength);
}

// Query the signal strength via a specific usb endpoint.  For the moment, we
// assume the endpoint is bidirectional, and derive the recv address from the
// xmit address.

static bool FwEpQuery( int Interface, int Endpoint, bptr XmitData, bit16 XmitLength) {
  bool rc = false;
  int err;
  if (Interface < 0) {
    printf( "  Using endpoint %d.\n", Endpoint);
  } else {
    printf( "  Using endpoint %d on interface %d.\n", Endpoint, Interface);
    usb_detach_kernel_driver_np( UsbDevHandle, Interface); // ignore errors
    if (err = usb_claim_interface( UsbDevHandle, Interface)) {
      printf( "* Claim interface error %d!\n", err);
  } }
  int EndpointXmit = Endpoint       ;
  int EndpointRecv = Endpoint | 0x80;
  usb_clear_halt( UsbDevHandle, EndpointXmit);
  usb_clear_halt( UsbDevHandle, EndpointRecv);
  if (FwEpMsgXmit( EndpointXmit, XmitData, XmitLength)) {
    rc = FwEpMsgRecv( EndpointRecv, XmitData);
  }
  if (Interface >= 0) {
    if (err = usb_release_interface( UsbDevHandle, Interface)) {
      printf( "* Release interface error %d!\n", err);
  } }
  return rc;
}

// Query messages for various modem types.

static byte FwEpGetDbm301[] = { // 2 bytes, unescaped, without crc or framing
  0xcc, 0x00
};

static byte FwEpGetDbm600[] = { // 18 bytes, unescaped, without crc or framing
  0x00, 0x0b, 0x00, 0x0d, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x05, 0x00, 0x01, 0x7c, 0xe4, 0x07
};

//-----------------------------------------------------------------------------
// Qualcomm/Novatel Support
//-----------------------------------------------------------------------------

// There seems to be an artificial 64-byte limit to an individual block write,
// so we will transparently implement that here.  We will also escape special
// characters as needed and add the frame end mark.

static bool NtPutBuffer( int PortHandle, bptr RawData, int RawLength) {
  if (PortHandle > 0) {
    static char EscData[256];
    int EscLength = 0;
    for (int i = 0; i < RawLength; i++) {
      switch (RawData[i]) {
        case 0x7d: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5d; break;
        case 0x7e: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5e; break;
        default  : EscData[EscLength++] = RawData[i];
    } }
    EscData[EscLength++] = 0x7e;
    int Block = (EscLength > 64) ? 64 : EscLength;
    int EscOffset = 0;
    while (EscLength && (write( PortHandle, EscData+EscOffset, Block) == Block)) {
      EscOffset += Block;
      EscLength -= Block;
      Sleep_mS( 1);
      if (Block > EscLength) {
        Block = EscLength;
    } }
    if (EscLength == 0) {
      return true;
  } }
  return false;
}

// Return true if Data is a valid message with correct crc and a
// frame end mark.

static bool NtMsgCheck( bptr RecvData, bit16 RecvLength) {
  if (RecvLength <= 3) return false;
  bit16 crc= Crc16( RecvData, RecvLength-3);
  byte CrcLo = crc & 0xff;
  byte CrcHi = crc >> 8;
  byte Frame = 0x7e;
  if (RecvData[RecvLength-3] != CrcLo) return false;
  if (RecvData[RecvLength-2] != CrcHi) return false;
  if (RecvData[RecvLength-1] != Frame) return false;
  return true;
}

// Put a byte into the receive buffer, un-escaping it as necessary.
// The escape transformations are:
//
//    7d 5e -> 7e
//    7d 5d -> 7d

#define NtRspLen_Rssi 116 // includes crc/eof
#define NtRspOff_Rssi  13

#define NtRspLen_Dbm   15 // includes crc/eof
#define NtRspOff_Dbm   10

static bool NtGetByte( byte RecvByte, bptr XmitData, bit16 XmitLength) {
  bool rc = false;
  if (RecvEscape) {
    RecvEscape = false;
    if (RecvByte == 0x5d) RecvByte = 0x7d;
    if (RecvByte == 0x5e) RecvByte = 0x7e;
  } else {
    if (RecvByte == 0x7d) {
      RecvEscape = true;
      return rc;
  } }
  if (RecvLength < sizeof( RecvData)) {
    RecvData[RecvLength++] = RecvByte;
  }
  if (RecvByte == 0x7e) {
    HexDump( "Xmit", XmitData, XmitLength);
    HexDump( "Recv", RecvData, RecvLength);
    if (NtMsgCheck( RecvData, RecvLength)) {
      switch (RecvLength) {
        case NtRspLen_Rssi:
          if (memcmp( RecvData, XmitData, 4) == 0) {
            bit16 Csq = RecvData[NtRspOff_Rssi];
            rc = ExportCsq( Csq);
          }
          break;
        case NtRspLen_Dbm:
          if (memcmp( RecvData, XmitData, 4) == 0) {
            bit16 Dbm = RecvData[NtRspOff_Dbm];
            rc = ExportDbm( Dbm);
          }
    } }
    RecvFlush();
  }
  return rc;
}

// Xmit a message after calculating and appending the crc.  The
// NtPutBuffer routine will escape and frame it.

static bool NtMsgXmit( int Modem, bptr XmitData, bit16 XmitLength) {
  static byte CrcData[256];
  memcpy( CrcData, XmitData, XmitLength);
  bit16 crc= Crc16( XmitData, XmitLength);
  CrcData[XmitLength++] = crc & 0xff;
  CrcData[XmitLength++] = crc >> 8;
  return NtPutBuffer( Modem, CrcData, XmitLength);
}

// Receive a message.  Processing will occur at a low level when the frame
// end character is received.

static bool NtMsgRecv( int Modem, bptr XmitData, bit16 XmitLength) {
  byte RecvByte;
  RecvFlush();
  while (SerialGet( Modem, RecvByte, 10)) {
    if (NtGetByte( RecvByte, XmitData, XmitLength)) {
      return true;
  } }
  return false;
}

// Qualcomm/Novatel QCDM protocol values (for documentation only)

#define QualcommDmSubSys 0x4b

#define NtChipset6800  0xfa
#define NtChipset6500  0x32

#define NtSnapshotSubCmd  0x0007

#define NtEvdo   0x07
#define NtWcdma  0x14

// Qualcomm/Novatel snapshot query messages for chipset/technology combinations:

static byte NtGetRssiEv65[] = { 0x4b, 0x32, 0x07, 0x00, 0x07, 0xff, 0xff };
static byte NtGetRssiWc65[] = { 0x4b, 0x32, 0x07, 0x00, 0x14, 0xff, 0xff };
static byte NtGetRssiEv68[] = { 0x4b, 0xfa, 0x07, 0x00, 0x07, 0xff, 0xff };
static byte NtGetRssiWc68[] = { 0x4b, 0xfa, 0x07, 0x00, 0x14, 0xff, 0xff };

static byte NtGetDbm[142] = {
  0x4b, 0xfa, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf1
};

// Discard all receive Data, then send a snapshot and interpret any response.

static bool NtQuery( int Modem, bptr XmitData, int XmitLength) {
  byte RecvByte;
  while (SerialGet( Modem, RecvByte, 10)) ;
  if (NtMsgXmit( Modem, XmitData, XmitLength)) {
    return NtMsgRecv( Modem, XmitData, XmitLength);
  }
  return false;
}

// Pantech UML290 Support
static byte PtSwitchto3G[21] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x7d, 0x24, 0x7d, 0x21, 0x7d, 0x21, 0x7d, 0x20,
  0x7d, 0x20, 0xce, 0xed, 0x0d
};

static byte PtSwitchto4G[21] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x7d, 0x24, 0x7d, 0x21, 0x7d, 0x34, 0x7d, 0x20,
  0x7d, 0x20, 0xe6, 0x51, 0x0d
};

static byte PtSwitchPart2[22] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x24, 0x7d, 0x23, 0x7d, 0x20, 0x7d, 0x20, 0x7d,
  0x20, 0x7d, 0x21, 0x3d, 0x41, 0x0d
};

static byte PtSwitchPart3[14] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x27, 0x7d, 0x20, 0x3c, 0x93, 0x0d
};

static byte PtSwitchPart4[14] = {
  0x41, 0x54, 0x2a, 0x57, 0x4d, 0x43, 0x3d, 0xc8, 0x27, 0x7d, 0x21, 0xb5, 0x82, 0x0d
};

static bool PtSwitch( int Modem, bptr XmitData, int XmitLength) {
  byte RecvByte;
  while (SerialGet( Modem, RecvByte, 10)) ;
  return write( Modem, XmitData, XmitLength);
}

//-----------------------------------------------------------------------------
// Sierra Wireless CnS Support
//-----------------------------------------------------------------------------

// There seems to be an artificial 64-byte limit to an individual block write,
// so we will transparently implement that here.  We will also escape special
// characters as needed and add the frame end mark.

static bool SwMsgXmit( int PortHandle, bptr RawData, int RawLength) {
  if (PortHandle > 0) {
    static byte EscData[256];
    int EscLength = 0;
    EscData[EscLength++] = 0x7e;
    for (int i = 0; i < RawLength; i++) {
      switch (RawData[i]) {
        case 0x7d: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5d; break;
        case 0x7e: EscData[EscLength++] = 0x7d; EscData[EscLength++] = 0x5e; break;
        default  : EscData[EscLength++] = RawData[i];
    } }
    EscData[EscLength++] = 0x7e;
    HexDump( "Xmit", EscData+1, EscLength-2);
    int Block = (EscLength > 64) ? 64 : EscLength;
    int EscOffset = 0;
    while (EscLength && (write( PortHandle, EscData+EscOffset, Block) == Block)) {
      EscOffset += Block;
      EscLength -= Block;
      Sleep_mS( 1);
      if (Block > EscLength) {
        Block = EscLength;
    } }
    if (EscLength == 0) {
      return true;
  } }
  return false;
}

// Put a byte into the receive buffer, un-escaping it as necessary.
// The escape transformations are:
//
//    7d 5e -> 7e
//    7d 5d -> 7d

#define SwBit16(x,y) ((x[y] << 8) | x[y+1])

// Supported cns object id codes

#define SwGetRssi   0x1001
#define SwGetActive 0x1009
#define SwGetEcio   0x1065

// Expected cns payload lengths

#define SwLenRssi   2
#define SwLenActive 2
#define SwLenEcio   8

static bool SwGetByte( byte RecvByte, bit16 CnsObjId, bit32 CnsAppId) {
  bool rc = false;
  if (RecvEscape) {
    RecvEscape = false;
    if (RecvByte == 0x5d) RecvByte = 0x7d;
    if (RecvByte == 0x5e) RecvByte = 0x7e;
  } else {
    if (RecvByte == 0x7d) {
      RecvEscape = true;
      return rc;
  } }
  if (RecvByte == 0x7e) {
    if (RecvLength > 12) {
      HexDump( "Recv", RecvData, RecvLength);
      bit16 CnsLength = SwBit16( RecvData, 0), HipLength = 0, PayLength = 0;
      if ((CnsObjId == SwGetRssi  ) && (CnsLength == 10 + SwLenRssi  )) { HipLength = CnsLength + 4; PayLength = SwLenRssi  ; }
      if ((CnsObjId == SwGetActive) && (CnsLength == 10 + SwLenActive)) { HipLength = CnsLength + 4; PayLength = SwLenActive; }
      if ((CnsObjId == SwGetEcio  ) && (CnsLength == 10 + SwLenEcio  )) { HipLength = CnsLength + 4; PayLength = SwLenEcio  ; }
      if (RecvLength == HipLength) {
        if ((RecvData[ 2] ==                     0x6b ) && // hip host notification
            (RecvData[ 3] ==                     0x00 ) && // hip reserved
            (RecvData[ 4] == ((CnsObjId >>  8) & 0xff)) &&
            (RecvData[ 5] == ( CnsObjId        & 0xff)) &&
            (RecvData[ 6] ==                     0x02 ) && // cns reply
            (RecvData[ 7] ==                     0x00 ) && // cns reserved
            (RecvData[ 8] == ((CnsAppId >> 24) & 0xff)) &&
            (RecvData[ 9] == ((CnsAppId >> 16) & 0xff)) &&
            (RecvData[10] == ((CnsAppId >>  8) & 0xff)) &&
            (RecvData[11] == ( CnsAppId        & 0xff)) &&
            (RecvData[12] ==                     0x00 ) &&
            (RecvData[13] ==                PayLength )) {
          bptr CnsData = RecvData+14;
          switch (CnsObjId) {
            case SwGetRssi  : rc = SwExportRssi  ( SwBit16( CnsData, 0)); break;
            case SwGetActive: rc = SwExportActive( SwBit16( CnsData, 0)); break;
            case SwGetEcio  : rc = SwExportEcio  ( SwBit16( CnsData, 0),
                                                   SwBit16( CnsData, 2),
                                                   SwBit16( CnsData, 4),
                                                   SwBit16( CnsData, 6)); break;
    } } } }
    RecvFlush();
  } else {
    if (RecvLength < sizeof( RecvData)) {
      RecvData[RecvLength++] = RecvByte;
  } }
  return rc;
}

// Receive a message.  Processing will occur at a low level when the frame
// end character is received.

static bool SwMsgRecv( int Modem, bit16 CnsObjId, bit32 CnsAppId) {
  byte RecvByte;
  RecvFlush();
  while (SerialGet( Modem, RecvByte, 10)) {
    if (SwGetByte( RecvByte, CnsObjId, CnsAppId)) {
      return true;
  } }
  return false;
}

static bool SwQuery( int Modem, bit16 CnsObjId) {
  static byte HipData[50];
  bit32 CnsAppId = time( NULL) ^ CnsObjId;
  byte RecvByte;
  bit16 HipLength = 0;

  HipData[HipLength++] = 0x00; // hip length placeholder
  HipData[HipLength++] = 0x00; // hip length placeholder
  HipData[HipLength++] = 0x2b; // hip modem notification
  HipData[HipLength++] = 0x00; // hip reserved

  bit16 CnsOffset = HipLength;

  HipData[HipLength++] = (CnsObjId >>   8) & 0xff;
  HipData[HipLength++] =  CnsObjId         & 0xff;
  HipData[HipLength++] = 0x01; // cns get
  HipData[HipLength++] = 0x00; // cns reserved
  HipData[HipLength++] = (CnsAppId >> 24) & 0xff;
  HipData[HipLength++] = (CnsAppId >> 16) & 0xff;
  HipData[HipLength++] = (CnsAppId >>  8) & 0xff;
  HipData[HipLength++] =  CnsAppId        & 0xff;
  HipData[HipLength++] = 0x00; // cns reserved
  HipData[HipLength++] = 0x00; // cns parameter length

  bit16 CnsLength = HipLength - CnsOffset;

  HipData[0] = (CnsLength >> 8) & 0xff;
  HipData[1] =  CnsLength       & 0xff;

  while (SerialGet( Modem, RecvByte, 10)) ;
  if (SwMsgXmit( Modem, HipData, HipLength)) {
    return SwMsgRecv( Modem, CnsObjId, CnsAppId);
  }
  return false;
}

//-----------------------------------------------------------------------------
// Determine which type of modem is istalled.
//-----------------------------------------------------------------------------

#define SwCdmaCns 1

#define NtCdmaDm  2

#define FwCdma301 31
#define FwCdma600 32

#define PtHybrid  41
#define PtUml290  42

static int GetModemType( ccptr & UsbPort) {
  UsbPort = "/dev/ttyUSB1";
  if (fptr f = fopen( "/tmp/value.modem", "r")) {
    static char Modem[20];
    if (fgets( Modem, sizeof( Modem), f)) {
      Modem[sizeof(Modem)-1] = 0;
      if (int n = strlen( Modem)) {
        while (n && !(Modem[n-1] & 0xe0)) Modem[--n] = 0;
      }
      printf( "Modem [%s]\n", Modem);
      if (strcmp( Modem, "sw250") == 0)                             return SwCdmaCns;
      if (strcmp( Modem, "sw597") == 0)                             return SwCdmaCns;
      if (strcmp( Modem, "sw598") == 0)                             return SwCdmaCns;
      if (strcmp( Modem, "hw366") == 0) { UsbPort = "/dev/ttyUSB0"; return SwCdmaCns; }
      if (strcmp( Modem, "nt727") == 0)                             return NtCdmaDm ;
      if (strcmp( Modem, "nt760") == 0)                             return NtCdmaDm ;
      if (strcmp( Modem, "fw301") == 0) { UsbPort = "/dev/ttyUSB2"; return FwCdma301; }
      if (strcmp( Modem, "fw600") == 0) { UsbPort = "/dev/ttyUSB2"; return FwCdma600; }
      if (strcmp( Modem, "pt290") == 0) { UsbPort = "/dev/ttyUSB2"; return PtUml290 ; }
      if (strcmp( Modem, "pt190") == 0) { UsbPort = "/dev/ttyACM0"; return PtHybrid ; }
      if (strcmp( Modem, "pt185") == 0) { UsbPort = "/dev/ttyACM0"; return PtHybrid ; }
      if (strcmp( Modem, "nt551") == 0) { UsbPort = "/dev/ttyUSB0"; return PtHybrid ; }
   // if (strcmp( Modem, "fw301") == 0) return NtCdmaDm ;
    }
    fclose( f);
  }
  return 0;
}

//-----------------------------------------------------------------------------
// Identify a usb device channel with the target pid/vid and open it.  This is
// the initial call when using libusb.  Returns a non-NULL device handle on
// success.
//-----------------------------------------------------------------------------

static bool LibUsbSeek( int Vid, int Pid) {
  usb_init();
  usb_find_busses();
  usb_find_devices();
  printf( "  Scanning for Vid:%4.4x Pid:%4.4x...\n", Vid, Pid);
  for (struct usb_bus * bus = usb_get_busses(); bus; bus = bus->next) {
    for (struct usb_device * dev = bus->devices; dev; dev = dev->next) {
      if ((dev->descriptor.idVendor == Vid) && (dev->descriptor.idProduct == Pid)) {
        UsbDevice = dev;
        printf( "  Device found.\n");
  } } }
  if (UsbDevice) {
    if (UsbDevHandle = usb_open( UsbDevice)) {
#ifdef USBX
      usbx_open( UsbDevice->bus->dirname, UsbDevice->filename);
#endif
      return true;
    }
    printf( "* Device open failed!\n");
  } else {
    printf( "* Device not found!\n");
  }
  return false;
}

//-----------------------------------------------------------------------------
// Trim leading and trailing whitespace from a buffer in-place and return a
// pointer to the modified buffer.
//-----------------------------------------------------------------------------

static ccptr Trim( ccptr Value) {
  cptr TrimValue = NULL;
  if (Value) {
    if (TrimValue = strdup( Value)) {
      cptr t = TrimValue;
      cptr s = TrimValue;
      while (*s && !(*s & 0xE0)) s++;
      while (*t++ = *s++) ;
      int n = strlen( TrimValue);
      while (n && !(TrimValue[n-1] & 0xE0)) TrimValue[--n] = 0;
  } }
  return TrimValue;
}

//-----------------------------------------------------------------------------
// Given a hex character (mixed case), return a 4-bit value 0..15 or -1 for
// error (hex digit invalid).
//-----------------------------------------------------------------------------

static int HexNibble( char x) {
  if (x) {
    if (strchr( "abcdef"    , x)) return x - 'a' + 10;
    if (strchr( "ABCDEF"    , x)) return x - 'A' + 10;
    if (strchr( "0123456789", x)) return x - '0'     ;
  }
  return -1;
}

//-----------------------------------------------------------------------------
// Given an unsigned hex string (mixed case, with optional leading whitespace
// and 0x prefix), return a 16-bit integer value from 0..65536 or -1 for
// error (leading hex digit invalid or null pointer).
//-----------------------------------------------------------------------------

static int HexBit16( ccptr x) {
  if (x) {
    while (*x == ' ') x++;
    int Nibble, Value = HexNibble( *x++);
    if (strncmp( x, "0x", 2) == 0) x += 2;
    if (Value >= 0) {
      do {
        Nibble = HexNibble( *x++);
        if (Nibble >= 0) {
          Value <<= 4;
          Value += Nibble;
        }
      } while (Nibble >= 0);
      return Value;
  } }
  return -1;
}

//-----------------------------------------------------------------------------
// Return the VID (vendor id) of the modem installed or -1 on error.
//-----------------------------------------------------------------------------

static int GetModemVid( void) {
  ccptr Value = NULL;
  if (fptr f = fopen( "/tmp/value.modem_vid", "r")) {
    static char Buffer[20];
    strcpy( Buffer, "");
    if (fgets( Buffer, sizeof( Buffer), f)) {
      Value = Trim( Buffer);
    }
    fclose( f);
  }
  if (Value) {
    if (strlen( Value) == 4) {
      return HexBit16( Value);
  } }
  return -1;
}

//-----------------------------------------------------------------------------
// Return the PID (product id) of the modem installed or -1 on error.
//-----------------------------------------------------------------------------

static int GetModemPid( void) {
  ccptr Value = NULL;
  if (fptr f = fopen( "/tmp/value.modem_pid", "r")) {
    static char Buffer[20];
    strcpy( Buffer, "");
    if (fgets( Buffer, sizeof( Buffer), f)) {
      Value = Trim( Buffer);
    }
    fclose( f);
  }
  if (Value) {
    if (strlen( Value) == 4) {
      return HexBit16( Value);
  } }
  return -1;
}

static int ModemVid = -1;
static int ModemPid = -1;

//-----------------------------------------------------------------------------
// Try appropriate query commands for different modems, chipsets and wireless
// technologies.
//-----------------------------------------------------------------------------

int main( int argc, ccptr argv[]) {
  int rc = 1;
  ccptr UsbPort = "/dev/ttyUSB1";
  const char * Mode = "";
  int ModemType;
  if (ModemType = GetModemType( UsbPort)) {

    // If the user has manually entered command-line parameters, try to honor
    // them.

    if (argc > 1) {
      if (strcmp( argv[1], "debug") == 0) {
        return DebugPrint();
      }
      if (strncmp( argv[1], "/dev/", 5) == 0) {
        UsbPort = argv[1];
        if (argc > 2) Mode = argv[2];
      } else {
        ModemVid = HexBit16( Trim( argv[1]));
        ModemPid = HexBit16( Trim( argv[2]));
                      if (ModemVid < 0) ModemVid = GetModemVid();
        if (argc > 2) if (ModemPid < 0) ModemPid = GetModemPid();
        if ((ModemVid >= 0) && (ModemPid >= 0)) {
          LibUsbSeek( ModemVid, ModemPid); // sets UsbDevHandle on success
    } } }

    // If the user didn't override things and we have one of the Franklin
    // modems, switch to endpoint mode automatically.

    if (!UsbDevHandle) {
      switch( ModemType) {
        case FwCdma301:
        case FwCdma600:
          ModemVid = GetModemVid();
          ModemPid = GetModemPid();
          if ((ModemVid >= 0) && (ModemPid >= 0)) {
            LibUsbSeek( ModemVid, ModemPid); // sets UsbDevHandle on success
          }
          break;
    } }
    

    // Do the query in either endpoint mode or serial device mode.

    if (UsbDevHandle) {
      switch( ModemType) {
        case FwCdma301:
          if (rc && FwEpQuery(  2, 4, FwEpGetDbm301, sizeof( FwEpGetDbm301))) rc = 0;
          break;
        case FwCdma600:
          if (rc && FwEpQuery( -1, 9, FwEpGetDbm600, sizeof( FwEpGetDbm600))) rc = 0;
          break;
      }
#ifdef USBX
      usbx_close(); // closes usbx_fd
#endif
      usb_close( UsbDevHandle);
    } else {
      int ModemPort = 0;
      if (SerialOpen( ModemPort, UsbPort)) {
        switch( ModemType) {
          case NtCdmaDm:
            if (rc && NtQuery( ModemPort, NtGetRssiEv68, sizeof( NtGetRssiEv68))) rc = 0;
            if (rc && NtQuery( ModemPort, NtGetRssiWc68, sizeof( NtGetRssiWc68))) rc = 0;
            if (rc && NtQuery( ModemPort, NtGetRssiEv65, sizeof( NtGetRssiEv65))) rc = 0;
            if (rc && NtQuery( ModemPort, NtGetRssiWc65, sizeof( NtGetRssiWc65))) rc = 0;
            if (rc) {
              if (NtQuery( ModemPort, NtGetDbm, sizeof( NtGetDbm))) rc = 0;
            } else {
                  NtQuery( ModemPort, NtGetDbm, sizeof( NtGetDbm));
            }
            break;
          case SwCdmaCns:
            if (SwQuery( ModemPort, SwGetActive)) rc = 0;
            if (SwQuery( ModemPort, SwGetRssi  )) rc = 0;
            if (SwQuery( ModemPort, SwGetEcio  )) rc = 0;
            break;
          case FwCdma301:
          case FwCdma600:
            if (FwQuery( ModemPort, FwGetDbm, sizeof( FwGetDbm))) rc = 0;
            break;
          case PtUml290:
              // TO-DO:  Find a way to get qmodem to either perform a switch to 3G or 4G
              // based on what mode the modem is currently in
            if (strcmp( Mode, "3g") == 0) {
              if (PtSwitch( ModemPort, PtSwitchto3G,  sizeof( PtSwitchto3G)))  rc = 0;
              if (PtSwitch( ModemPort, PtSwitchPart2, sizeof( PtSwitchPart2))) rc = 0;
              if (PtSwitch( ModemPort, PtSwitchPart3, sizeof( PtSwitchPart3))) rc = 0;
              if (PtSwitch( ModemPort, PtSwitchPart4, sizeof( PtSwitchPart4))) rc = 0;
            }
            if (strcmp( Mode, "4g") == 0) {
              if (PtSwitch( ModemPort, PtSwitchto4G,  sizeof( PtSwitchto4G)))  rc = 0;
              if (PtSwitch( ModemPort, PtSwitchPart2, sizeof( PtSwitchPart2))) rc = 0;
              if (PtSwitch( ModemPort, PtSwitchPart3, sizeof( PtSwitchPart3))) rc = 0;
              if (PtSwitch( ModemPort, PtSwitchPart4, sizeof( PtSwitchPart4))) rc = 0;
            }
            break;
        }
        SerialClose( ModemPort);
      } else {
        printf( "### Error opening [%s]\n", UsbPort);
    } }
  } else {
    printf( "### Unrecognized modem\n");
  }
  if (rc == 0 && ModemType != PtUml290) {
    ExportCsqDbmSignal();
    system( "rm -f /tmp/flag.rssi_err");
    if (fptr f = fopen( "/tmp/value.rssi_sig", "w")) {
      fprintf( f, "%s\n", SigRc); fclose( f);
  } }
  return rc;
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

